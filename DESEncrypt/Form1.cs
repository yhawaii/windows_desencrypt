﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DESEncrypt
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Click(object sender, EventArgs e)
        {
            string strKey = txtKey.Text.Trim();
            string strWorld = txtWorld.Text.Trim();
            if (string.IsNullOrEmpty(strKey))
            {
                MessageBox.Show("请输入Key值");
                return;
            }

            if (string.IsNullOrEmpty(strWorld))
            {
                MessageBox.Show("请输入加密/解密字符");
                return;
            }

            Button btn = sender as Button;
            if (btn != null)
            {
                switch (btn.Name)
                {
                    case "btnEncrypt":
                        lblWorld.Text = clsDESEncrypt.Encrypt(strWorld, strKey);
                        break;
                    case "btnDecrypt":
                        lblWorld.Text = clsDESEncrypt.Decrypt(strWorld, strKey);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
